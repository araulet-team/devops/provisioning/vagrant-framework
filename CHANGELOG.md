# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

<!--- next entry here -->

## 0.4.0
2020-05-16

### Features

- upgrade software version + fix Ansible runtime (5666de31424bc02bdf9ed1c78896053c041b568e)

## 0.3.3
2019-09-08

### Fixes

- **vagrant:** show gui (a05e1aa3a76c72b75ed5e1c5c6aad15e9dd0299f)

## 0.3.2
2019-09-08

### Fixes

- **ansible:** init script + configuration (ad4bffb20be6d0cd725969e9eddb84f27614d513)

## 0.3.1
2019-09-06

### Fixes

- upgrade ubuntu 18 desktop image + remove skip tag git (f2e253ad0f28141a50694a77f50bc20f45720752)

## 0.3.0
2019-09-06

### Features

- **ansible:** decouple vars template from the one really used (a14b846aba8da46edb2bca1b5a9a6b6ead1b4ca2)

## 0.2.2
2019-09-06

### Fixes

- **ansible:** change playbook name araulet to common (a9a042640eb0f2ffaab91ba4f0680bfd4665f6df)

## 0.2.1
2019-09-01

### Fixes

- **git:** path to dl repositories: (da2fae692037607c5654466eff3120c33a385438)

## 0.2.0
2019-08-31

### Features

- add zsh role (b748e9e6307bb7e36f7a5d23481d7abcdc265263)
- change workflow to control plays (c0531198fab2e528ad7b8bbc08660892c42c6b59)
- **ci:** add versioning and changelog (b1889fdf9f32e2f9f033ec725289066d16db3301)

### Fixes

- plays order (f796fc454e010223b3dd645e97cd24636991ab5e)
- vb-guest plugin (5308223146694017cb759569ea6825116ed9ce2a)
- **vagrant:** remove excution cap (a92b3b235318e7ef6cdfbef4a42e424c566cd575)
- graphiccontroller + accelerate3d (f796df5659dde288475f995faf3bcbf0aa6dc1ca)
- **git:** use https over git:// (e4eca7321cb62ae346ed936567dfda35572be0b4)
- type in software plays (e7f8dd1f09b939cfa4ca15f6ac8bbd1b544d6bd2)

