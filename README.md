# Vagrant [![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-yellow.svg)](https://conventionalcommits.org)

Start boxes with [Vagrant](https://www.vagrantup.com/) and provision them with [Ansible](https://www.ansible.com/)

## Contents

* [Prerequisites](#prerequisites)
* [Getting Started](#getting-started)
* [Troubleshooting](#troubleshooting)

## Prerequisites

*software that needs to be installed*

* [Virtualbox 6.0.6](https://www.virtualbox.org/wiki/Downloads)
* [Vagrant 2.2.4](https://www.vagrantup.com/intro/getting-started/install.html)

**optional**
* [Ansible 2.7.x](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html): *not needed, if you only provision via vagrant.*

##### `People can encounter a lot of bugs when provisioning their machines, I recommend to stay with these versions.`

## Getting Started

### Initial Setup

Go **[there](./docs/1.initial-setup.md)** to understand project structure and configure your vagrant.

### Vagrant Provisioning

Go **[there](./docs/2.vagrant-provisioning.md)** and understand how provisioning is working.

### Vagrant Lifecycle

Go **[there](./docs/3.vagrant-lifecycle.md)** to start your box etc.

## Troubleshooting

See [known issues](./docs/4.known-issues.md)
