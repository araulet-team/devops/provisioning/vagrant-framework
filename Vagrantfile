# -*- mode: ruby -*-
# vi: set ft=ruby :
require 'yaml'

Vagrant.require_version ">= 2.0.0"

# -----------------------------
# load additional vagrant plugin
# -----------------------------
required_plugins = %w[vagrant-vbguest vagrant-proxyconf nugrant]
plugins_to_install = required_plugins.reject { |plugin| Vagrant.has_plugin? plugin }
unless plugins_to_install.empty?
  puts "Installing plugins: #{plugins_to_install.join(' ')}"
  if system "vagrant plugin install #{plugins_to_install.join(' ')}"
    exec "vagrant #{ARGV.join(' ')}"
  else
    abort 'Installation of one or more plugins has failed. Aborting.'
  end
end

Vagrant.configure(2) do |config|
  clusters_default = YAML.load_file(File.join(File.dirname(__FILE__), 'boxes.default.yml'))
  clusters = YAML.load_file(File.join(File.dirname(__FILE__), config.user.boxes_definition))

  # -----------------------------
  # proxy setup
  # -----------------------------
  if Vagrant.has_plugin?("vagrant-proxyconf")
    config.proxy.enabled  = config.user.proxy.enabled
    config.proxy.http     = config.user.proxy.http
    config.proxy.https    = config.user.proxy.https
    config.proxy.no_proxy = config.user.proxy.no_proxy
    config.proxy.ftp      = config.user.proxy.ftp
  end

  # bug with 6.0.6 https://github.com/dotless-de/vagrant-vbguest/issues/333
  # To be removed when fix will be available

  # if Vagrant.has_plugin?("vagrant-vbguest")
  #   config.vbguest.auto_update = false
  # end

  clusters.each_with_index do |(opts), index|
    # merge with default
    opts = clusters_default.merge(opts)

    if opts['enabled']
      # each node represent one machine
      config.vm.define opts['host'] do |node|

        # -----------------------------
        # 1.configure box
        # -----------------------------
        node.vm.box = opts['box']['name']
        node.vm.box_version = opts['box']['version']

        # -----------------------------
        # 2.mount
        # vagrant folder is mount by
        # default "/vagrant/"
        # -----------------------------
        if opts['shared']['enabled']
          node.vm.synced_folder "#{ENV['HOME']}/#{opts['shared']['host_folder']}",
                                "/home/vagrant/#{opts['shared']['guest_folder']}"
        end

        node.vm.provider "virtualbox" do |vb, override|
          # -----------------------------
          # 3. configure internal network
          # -----------------------------
          if opts['networks_interface']
            opts['networks_interface'].each do |interface|
              override.vm.network "private_network", ip: "#{interface['ip']}",
                virtualbox__intnet: "intnet-#{interface['intnet']}"
            end
          end

          # -----------------------------
          # 4. configure vb instance
          # -----------------------------
          vb.name = "#{opts['host']}"

          if opts['gui']
            vb.gui = "#{opts['gui']}"
          end

          vb.customize [
            "modifyvm", :id,
            "--groups", opts['group'],
            "--memory", opts['mem'],
            "--cpus", opts['cpus'],
            "--vram", opts['vram'],
            "--clipboard", "bidirectional",
            "--draganddrop", "bidirectional",
            "--monitorcount", opts['monitorcount'],
            "--graphicscontroller", opts['graphicscontroller'],
            "--accelerate3d", opts['accelerate3d'],
            # "--cpuexecutioncap", "50",
          ]

          # -------------------------------
          # 5. configure port forwarding
          # (override the default ssh port)
          # -------------------------------
          if opts['forwarded_port']
            override.vm.network "forwarded_port", guest: 22, host: "#{opts['forwarded_port']}", id: "ssh"
          end

        end

        # -------------------------------
        # 6. setup ssh key
        # Needed for ansible to
        # target private repositories
        # -------------------------------
        if config.user.ssh_key_enabled
          node.vm.provision "file", source: "#{config.user.ssh_key_private_path}", destination: "~/.ssh/id_rsa"
          node.vm.provision "file", source: "#{config.user.ssh_key_public_path}", destination: "~/.ssh/id_rsa.pub"

          $script_ssh_config = <<-SCRIPT
            echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> /home/vagrant/.ssh/config
            chown vagrant:vagrant /home/vagrant/.ssh/config
            chmod 600 /home/vagrant/.ssh/id_rsa*
            eval $(ssh-agent -s)
            chmod 700 /home/vagrant/.ssh
          SCRIPT

          node.vm.provision "shell", inline: $script_ssh_config, privileged: true
          node.vm.provision "shell", inline: "ssh -T #{config.user.ssh_authenticate_to}", privileged: false
        end

        # https://www.vagrantup.com/docs/provisioning/ansible_common.html
        node.vm.provision 'ansible_local' do |ansible|
          ansible.compatibility_mode = "2.0"
          ansible.verbose = config.user.ansible.verbose
          ansible.config_file = config.user.ansible.config_file
          ansible.playbook = config.user.ansible.playbook_path
          ansible.inventory_path = config.user.ansible.inventory_path
          ansible.galaxy_role_file = config.user.ansible.galaxy_role_file
          ansible.galaxy_roles_path = config.user.ansible.galaxy_roles_path
          ansible.galaxy_command = 'ansible-galaxy install --force --role-file=%{role_file} --roles-path=%{roles_path}'
          ansible.skip_tags = config.user.ansible.skip_tags
          ansible.tags = config.user.ansible.tags
          ansible.limit = "#{opts['host']}"
          # ansible.install_mode = "pip"
          # ansible.version = user.config.version
        end
      end
    end

  end

end
