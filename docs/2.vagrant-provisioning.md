# Vagrant Provisioning

Vagrant use Ansible to provision your machine.

Playbooks availables are located in **./ansible/** folder. By default the one  executed is **common**.

This playbook contains all roles listed [here](https://gitlab.com/araulet-team/devops/ansible) and specific configuration is driven by the file **./ansible/common/var.defaults.yaml**.

### How Vagrant and Ansible are working together ?

Vagrant is using [`ansible-local`](https://www.vagrantup.com/docs/provisioning/ansible_local.html).

    The Vagrant Ansible Local provisioner allows you to provision the guest using Ansible playbooks by executing ansible-playbook directly on the guest machine.

### Skipping playbook

There is a lot of way to skip some tasks/plays depending of how you execute Ansible.

1) `vagrant up --no-provision`: to skip everything

2) edit `.vagrantuser` properties **skip_tags** or **tags** to adjust what you want to target.
    - see file **./ansible/common/main.yml** for existing tags that you can use.
    * official documentation (https://docs.ansible.com/ansible/2.7/user_guide/playbooks_tags.html)
