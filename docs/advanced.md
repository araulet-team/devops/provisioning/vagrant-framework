# Advanced Settings

#### machines configuration

All your machines are defined in the file `boxes.yml`. it's an array of object.

**Options that you can define**

| variable | mandatory | note |
| ---------| --------- | ---- |
| **host**              | yes | vagrant hostname |
| **enabled**           | yes | you can disable the box from building |
| **box.name**          | yes | box name on vagrant cloud |
| **box.version**       | yes | box version |
| **cpus**              | yes | |
| **mem**               | yes | |
| **vram**              | yes | |
| **monitorcount**      | yes | number of monitor available 
| **group**             | yes | machines are going to be grouped on VB ui |
| **forwarded_port**    | no | ssh port - default to 22 |
| **networks_interface.ip**     | no | give a ip to your machine |
| **networks_interface.intnet** | no | |
| **shared**              | yes |  |
| **shared.enabled**      | yes | enabled shared folder |
| **shared.host_folder**  | yes | host folder to shared |
| **shared.guest_folder** | yes | guest folder destination |

#### user configuration

user configuration is defined in `.vagrantuser` file. 
Global configuration applying to all boxes when using `vagrant up --provision`.

| variable | note | default |
| -------- | ---- | ------- |
| **proxy**                     | proxy configuration | `{}` |
| **proxy.enabled **            |   | `false` |
| **proxy.http**                |   | `""` |
| **proxy.https**               |   | `""` |
| **proxy.ftp**                 |   | `""` |
| **proxy.no_proxy**            |   | `""` |
| **boxes_definition**          | path to your boxes definition | `boxes.example.yml` |
| **ansible**                   | Ansible provisioner configuration  | `{}` |
| **ansible.verbose**           | add verbosity when running Ansible | `v` |
| **ansible.config_file**       | ansible config   | `/vagrant/ansible/example/ansible.cfg` |
| **ansible.inventory_file**    | inventory file | `/vagrant/ansible/example/inventories/local/guest` |
| **ansible.playbook_file**     | playbook file | `/vagrant/ansible/example/example.yml` |
| **ansible.galaxy_roles_path** | path to your folder roles | `/vagrant/ansible/example/.tmp/roles` |
| **ansible.galaxy_role_file**  | dependencies list | `/vagrant/ansible/example/requirements.yml` |
| **ansible.skip_tags**         | list of tags you want to skip | `[]` |
| **ansible.tags**              | list of tags you want to execute | `[]` |
| **ssh_key_enabled**           | put your ssh keys in your machine. Needed because of Ansible | `false` |
| **ssh_key_private_path**      | path to private key | |
| **ssh_key_public_path**       | path to public key | |
| **ssh_authenticate_to**       | e.g. `git@github.com` | |
