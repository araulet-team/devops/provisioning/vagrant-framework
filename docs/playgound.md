# Playground [WIP]

How to use and play with the current example provided.

## Prerequisites

##### `Ansible needs to be installed on your machine.`

## Execute Ansible on the Guest machine

#### ping

```shell
ANSIBLE_CONFIG=ansible/common/ansible.cfg ansible desktop02 -i ansible/common/inventories/local/guest -m ping
```

#### script

```shell
ANSIBLE_CONFIG=ansible/common/ansible.cfg ansible desktop02 -i ansible/common/inventories/local/host -m script -a 'path to your script'
```
